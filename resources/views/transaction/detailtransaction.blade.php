@extends('layouts.appAdmin')

@section('title')
    Halaman Detail Transaction
@endsection

@section('content')
<h1>{{$transact->dateOfTransaction}}</h1>
<p>{{$transact->quantity}}</p>
<p>{{$transact->total}}</p>
@endsection