@extends('layouts.appAdmin')

@section('title')
    Halaman Edit Transaction
@endsection

@section('content')
<form action="/transaction/{{$transact->id}}" method="POST"> 
  @csrf
  @method('PUT')
  @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

    <div class="form-group">
      <label>Name</label>
      <input type="text" name='dateOfTransaction' value="{{$transact->dateOfTransaction}}" class="form-control">
    </div>
    <div class="form-group">
        <label>Quantity</label>
        <textarea name="quantity" class="form-control" id="" cols="30" rows="10">{{$transact->quantity}}</textarea>
      </div>
    <div class="form-group">
      <label>total</label>
      <input type="text" name='total' value="{{$transact->total}}" class="form-control">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection