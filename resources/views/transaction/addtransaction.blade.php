@extends('layouts.appAdmin')

@section('title')
    Halaman Add Transaction
@endsection

@section('content')
<form action="/transaction" method="POST">
  @csrf
  @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="content-wrapper" class="d-flex flex-column">
    <label>dateOfTransaction</label>
    <input type="date" name='dateOfTransaction' class="form-control">
  </div>
  <div class="form-group">
    <label>Quantity</label>
    <input type="text" name='quantity' class="form-control">
  </div>
  <div class="form-group">
    <label>Total</label>
    <input type="text" name='total' class="form-control">
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection