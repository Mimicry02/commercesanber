@extends('layouts.appAdmin')

@section('title')
    Transaction
@endsection

@section('content')
<a href="/transaction/addtransaction" class="btn btn-primary my-3">Add Transaction</a>

<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">dateOfTransaction</th>
        <th scope="col">Quantity</th>
        <th scope="col">Total</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>

@forelse ($transaction as $key => $transact)

<tr>
    <th scope="row">{{$key+1}}</th>
    <td>{{$transact->dateOfTransaction}}</td>
    <td>{{$transact->quantity}}</td>
    <td>{{$transact->total}}</td>
    <td>
    <a href="/transaction{{$transact->id}}" class="btn btn-sm btn-info">Detail</a>
    <a href="/transaction/{{$transact->id}}/edit" class="btn btn-sm btn-warning">Edit</a>
        <form action="/transaction/{{$transact->id}}" method="POST">
        @csrf
        @method('delete')
        <input type="submit" value="Delete" class="btn btn-sm btn-danger">
        </form>
      </td>
</tr>

@empty
    <p>No users</p>
@endforelse

    </tbody>
  </table>
@endsection