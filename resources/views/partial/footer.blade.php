<footer class="sticky-footer bg-white">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
            <span>Copyright &copy; Sanber Commerce 20231</span>
        </div>
    </div>
</footer>