@extends('layouts.appAdmin')

@section('title')
    Items
@endsection

@section('content')
<a href="/items/additem" class="btn btn-primary my-3">Add Items</a>

<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Description</th>
        <th scope="col">Price</th>
        <th scope="col">thumbnail</th>
        <th scope="col">Stock</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>

@forelse ($items as $key => $items)

<tr>
    <th scope="row">{{$key+1}}</th>
    <td>{{$items->name}}</td>
    <td>{{$items->description}}</td>
    <td>{{$items->price}}</td>
    <td>{{$items->thumbnail}}</td>
    <td>{{$items->stock}}</td>
    <td>
    <a href="/item{{$items->id}}" class="btn btn-sm btn-info">Detail</a>
    <a href="/item/{{$items->id}}/edit" class="btn btn-sm btn-warning">Edit</a>
        <form action="/item/{{$items->id}}" method="POST">
        @csrf
        @method('delete')
        <input type="submit" value="Delete" class="btn btn-sm btn-danger">
        </form>
      </td>
</tr>

@empty
    <p>No users</p>
@endforelse

    </tbody>
  </table>
@endsection