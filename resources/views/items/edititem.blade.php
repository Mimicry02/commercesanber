@extends('layouts.appAdmin')

@section('title')
    Halaman Edit Items
@endsection

@section('content')
<form action="/items/{{$items->id}}" method="POST" enctype="multipart/form-data"> 
  @csrf
  @method('PUT')
  @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

    <div class="form-group">
      <label>Name</label>
      <input type="text" name='name' value="{{$items->name}}" class="form-control">
    </div>
    <div class="form-group">
        <label>Description</label>
        <textarea name="description" class="form-control" id="" cols="30" rows="10">{{$items->description}}</textarea>
      </div>
    <div class="form-group">
      <label>Price</label>
      <input type="text" name='price' value="{{$items->price}}" class="form-control">
    </div>
    <div class="form-group">
        <label>Thumbnail</label>
        <input type="file" name='thumbnail' value="{{$items->thumbnail}}" class="form-control">
      </div>
      <div class="form-group">
        <label>Stock</label>
        <input type="text" name='stock' value="{{$items->stock}}" class="form-control">
      </div>
      <div class="form-group">
        <label>Date Posted</label>
        <input type="text" name='datePosted' value="{{$items->datePosted}}" class="form-control">
      </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection