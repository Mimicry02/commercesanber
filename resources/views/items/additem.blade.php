@extends('layouts.appAdmin')

@section('title')
    Halaman Add Items
@endsection

@section('content')
<form action="/items" method="POST">
  @csrf
  @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="content-wrapper" class="d-flex flex-column">
    <label>Name</label>
    <input type="text" name='name' class="form-control">
  </div>
  <div class="form-group">
      <label>Description</label>
      <textarea name="description" class="form-control" id="" cols="30" rows="10"></textarea>
    </div>
  <div class="form-group">
    <label>Price</label>
    <input type="text" name='price' class="form-control">
  </div>
  <div class="form-group">
      <label>Thumbnail</label>
      <input type="file" name='thumbnail'class="form-control">
    </div>
    <div class="form-group">
      <label>Stock</label>
      <input type="text" name='stock'  class="form-control">
    </div>
    <div class="form-group">
      <label>Date Posted</label>
      <input type="date" name='datePosted'  class="form-control">
    </div>
    <div class="form-group">
        <label>Date Posted</label>
        <select name="category_id" class="form-control">
            <option value="">Pilih Category</option>
            @foreach ($categories as $item)
            @If ($item ->id == $items->category_id)
            <option value="{{$item ->id}}" selected>{{$item->name}}</option>
            @else
            <option value="{{$item->id}}">{{$item->name}}</option>
            @endforeach
        </select>
      </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection