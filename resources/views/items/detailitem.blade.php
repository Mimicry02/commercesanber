@extends('layouts.appAdmin')

@section('title')
    Halaman Bio Cast
@endsection

@section('content')
<h1>{{$items->name}}</h1>
<p>{{$items->description}}</p>
<p>{{$items->price}}</p>
<p>{{$items->thumbnail}}</p>
<p>{{$items->stock}}</p>
<p>{{$items->datePosted}}</p>
@endsection