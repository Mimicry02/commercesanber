@extends('layouts.appAdmin')

@section('title')
    Category
@endsection

@section('content')
<a href="/category/addcategory" class="btn btn-primary my-3">Add Category</a>

<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>

@forelse ($category as $key => $categories)

<tr>
    <th scope="row">{{$key+1}}</th>
    <td>{{$categories->name}}</td>
    <td>
    <a href="/category{{$categories->id}}" class="btn btn-sm btn-info">Detail</a>
    <a href="/category/{{$categories->id}}/edit" class="btn btn-sm btn-warning">Edit</a>
        <form action="/category/{{$categories->id}}" method="POST">
        @csrf
        @method('delete')
        <input type="submit" value="Delete" class="btn btn-sm btn-danger">
        </form>
      </td>
</tr>

@empty
    <p>No users</p>
@endforelse

    </tbody>
  </table>
@endsection