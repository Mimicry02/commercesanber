@extends('layouts.appAdmin')

@section('title')
    Halaman Add category
@endsection

@section('content')
<form action="/category" method="POST">
  @csrf
  @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="content-wrapper" class="d-flex flex-column">
    <label>Name</label>
    <input type="text" name='name' class="form-control">
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection