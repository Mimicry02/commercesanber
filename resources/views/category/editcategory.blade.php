@extends('layouts.appAdmin')

@section('title')
    Halaman Edit Category
@endsection

@section('content')
<form action="/category/{{$categories->id}}" method="POST"> 
  @csrf
  @method('PUT')
  @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

    <div class="form-group">
      <label>Name</label>
      <input type="text" name='name' value="{{$categories->name}}" class="form-control">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection