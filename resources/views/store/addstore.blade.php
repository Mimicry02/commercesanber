@extends('layouts.appAdmin')

@section('title')
    Halaman Add Store
@endsection

@section('content')
<form action="/store" method="POST">
  @csrf
  @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="content-wrapper" class="d-flex flex-column">
    <label>name</label>
    <input type="date" name='name' class="form-control">
  </div>
  <div class="form-group">
    <label>Bio</label>
    <textarea type="text" name='bio' cols="30" rows="10" class="form-control">
  </div>
  <div class="form-group">
    <label>Email</label>
    <input type="text" name='email' class="form-control">
  </div>
  <div class="form-group">
    <label>Address</label>
    <input type="text" name='address' class="form-control">
  </div>
  <div class="form-group">
    <label>Password</label>
    <input type="password" name='password' class="form-control">
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection