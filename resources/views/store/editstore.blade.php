@extends('layouts.appAdmin')

@section('title')
    Halaman Edit Store
@endsection

@section('content')
<form action="/store/{{$stored->id}}" method="POST"> 
  @csrf
  @method('PUT')
  @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

    <div class="form-group">
      <label>Name</label>
      <input type="text" name='name' value="{{$stored->name}}" class="form-control">
    </div>
    <div class="form-group">
        <label>Bio</label>
        <textarea name="description" class="form-control" id="" cols="30" rows="10">{{$stored->bio}}</textarea>
      </div>
    <div class="form-group">
      <label>Address</label>
      <input type="text" name='address' value="{{$stored->address}}" class="form-control">
    </div>
    <div class="form-group">
        <label>Email</label>
        <input type="file" name='email' value="{{$stored->email}}" class="form-control">
      </div>
      <div class="form-group">
        <label>Password</label>
        <input type="text" name='password' value="{{$stored->password}}" class="form-control">
      </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection