@extends('layouts.appAdmin')

@section('title')
    Store
@endsection

@section('content')
<a href="/store/addstore" class="btn btn-primary my-3">Add Store</a>

<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Name</th>
        <th scope="col">Bio</th>
        <th scope="col">Address</th>
        <th scope="col">Email</th>
        <th scope="col">Password</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>

@forelse ($stored as $key => $stored)

<tr>
    <th scope="row">{{$key+1}}</th>
    <td>{{$stored->name}}</td>
    <td>{{$stored->bio}}</td>
    <td>{{$stored->address}}</td>
    <td>{{$stored->email}}</td>
    <td>{{$stored->password}}</td>
    <td>
    <a href="/store{{$stored->id}}" class="btn btn-sm btn-info">Detail</a>
    <a href="/store/{{$stored->id}}/edit" class="btn btn-sm btn-warning">Edit</a>
        <form action="/store/{{$stored->id}}" method="POST">
        @csrf
        @method('delete')
        <input type="submit" value="Delete" class="btn btn-sm btn-danger">
        </form>
      </td>
</tr>

@empty
    <p>No users</p>
@endforelse

    </tbody>
  </table>
@endsection