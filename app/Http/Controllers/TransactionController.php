<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TransactionController extends Controller
{
    public function create(){
        return view("transaction.addtransaction");
    }

    public function store(Request $request){
        $request->validate([
            'dateOfTransaction' => 'required',
            'quantity' => 'required',
            'total' => 'required',
        ]);

        DB::table('transaction')->insert([
            'dateOfTransaction' => $request-> input('dateOfTransaction'),
            'quantity' => $request-> input('quantity'),
            'total' => $request-> input('total')
        ]);

        return redirect('/transaction');
    }

    public function index(){
        $transact= DB::table('transaction')->get();

        return view ('transaction.viewtransaction', ['transaction' => $transact]);
    }

    public function show($id){
        $transact = DB::table('transaction')->find($id);

        return view ('transaction.detailtransaction', ['transaction' => $transact]);
    }

    public function edit($id){

        $transact = DB::table('transaction')->find($id);

        return view ('transaction.edittransaction', ['transaction' => $transact]);
    }

    public function update($id, Request $request){

        $request->validate([
            'dateOfTransaction' => 'required',
            'quantity' => 'required',
            'total' => 'required',
    ]);

    DB::table('transaction')
              ->where('id', $id)
              ->update([
                'dateOfTransaction' => $request-> input('dateOfTransaction'),
                'quantity' => $request-> input('quantity'),
                'total' => $request-> input('total'),
              ]);

              return redirect ('/transaction');
}
    public function destroy($id){
        DB::table('transaction')->where('id', '=', $id)->delete();

        return redirect ('/transaction');
    }
}
