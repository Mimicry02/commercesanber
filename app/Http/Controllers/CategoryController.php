<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function create(){
        return view("category.addcategory");
    }

    public function store(Request $request){
        $request->validate([
            'name' => 'required|min:3'
        ]);

        DB::table('category')->insert([
            'name' => $request-> input('name')
        ]);

        return redirect('/category');
    }

    public function index(){
        $categories= DB::table('category')->get();

        return view ('category.viewcategory', ['category' => $categories]);
    }

    public function show($id){
        $categories = DB::table('category')->find($id);

        return view ('category.detailcategory', ['category' => $categories]);
    }

    public function edit($id){

        $categories = DB::table('category')->find($id);

        return view ('category.editcategory', ['category' => $categories]);
    }

    public function update($id, Request $request){
        $request->validate([
            'name' => 'required|min:3'
    ]);

    DB::table('category')
              ->where('id', $id)
              ->update([
                'name' => $request-> input('name')
              ]);

              return redirect ('/category');
}
    public function destroy($id){
        DB::table('category')->where('id', '=', $id)->delete();

        return redirect ('/category');
    }
}
