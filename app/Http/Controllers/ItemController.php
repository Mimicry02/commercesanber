<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class ItemController extends Controller
{
    public function create(){
        $categories = CategoryController::all();
        return view("items.addItem", ['categories' => $categories]);
    }

    public function store(Request $request){
        $request->validate([
            'name' => 'required|min:3',
            'description' => 'required',
            'price' => 'required',
            'thumbnail' => 'required',
            'stock' => 'required',
            'datePosted' => 'required',
            'category_id' => 'required'
        ]);

        DB::table('items')->insert([
            'name' => $request-> input('name'),
            'description' => $request-> input('description'),
            'price' => $request-> input('price'),
            'thumbnail' => $request-> input('thumbnail'),
            'stock' => $request-> input('stock'),
            'datePosted' => $request-> input('datePosted'),
            'category_id' => $request-> input('name')
        ]);

        return redirect('/items');
    }

    public function index(){
        $items= DB::table('items')->get();

        return view ('items.viewitem', ['items' => $items]);
    }

    public function show($id){
        $items = DB::table('items')->find($id);

        return view ('items.detailtem', ['items' => $items]);
    }

    public function edit($id){

        $items = DB::table('items')->find($id);

        return view ('items.edititem', ['items' => $items]);
    }

    public function update($id, Request $request){
        $request->validate([
            'name' => 'required|min:3',
            'description' => 'required',
            'price' => 'required',
            'thumbnail' => 'required',
            'stock' => 'required',
            'datePosted' => 'required',
            'category_id' => 'required'
    ]);

    DB::table('items')
              ->where('id', $id)
              ->update([
                'name' => $request-> input('name'),
                'description' => $request-> input('description'),
                'price' => $request-> input('price'),
                'thumbnail' => $request-> input('thumbnail'),
                'stock' => $request-> input('stock'),
                'datePosted' => $request-> input('datePosted'),
                'category_id' => $request-> input('name'),
              ]);

              return redirect ('/items');
}
    public function destroy($id){
        DB::table('items')->where('id', '=', $id)->delete();

        return redirect ('/items');
    }
}
