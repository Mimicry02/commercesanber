<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class StoreController extends Controller
{
    public function create(){
        return view("store.addstore");
    }

    public function store(Request $request){
        $request->validate([
            'name' => 'required',
            'bio' => 'required',
            'address' => 'required',
            'email' => 'required',
            'password' => 'required'
        ]);

        DB::table('store')->insert([
            'name' => $request-> input('name'),
            'bio' => $request-> input('bio'),
            'address' => $request-> input('address'),
            'email' => $request-> input('email'),
            'password' => $request-> input('password')
        ]);

        return redirect('/store');
    }

    public function index(){
        $stored= DB::table('store')->get();

        return view ('store.viewstore', ['store' => $stored]);
    }

    public function show($id){
        $stored = DB::table('store')->find($id);

        return view ('store.detailstore', ['store' => $stored]);
    }

    public function edit($id){

        $stored = DB::table('store')->find($id);

        return view ('store.editstore', ['store' => $stored]);
    }

    public function update($id, Request $request){

        $request->validate([
            'name' => 'required',
            'bio' => 'required',
            'address' => 'required',
            'email' => 'required',
            'password' => 'required'
    ]);

    DB::table('store')
              ->where('id', $id)
              ->update([
            'name' => $request-> input('name'),
            'bio' => $request-> input('bio'),
            'address' => $request-> input('address'),
            'email' => $request-> input('email'),
            'password' => $request-> input('password')
              ]);

              return redirect ('/store');
}
    public function destroy($id){
        DB::table('store')->where('id', '=', $id)->delete();

        return redirect ('/store');
    }
}
