<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class QuickController extends Controller
{
    public function about(){
        return view('layouts.about');
    }

    public function guest()
    {
        return view('layouts.appGuest');
    }

    public function register()
    {
        return view('auth.register');
    }

    public function login()
    {
        return view('auth.login');
    }
}
