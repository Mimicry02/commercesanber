<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function dashboard (){
        return view("dashboard");
    }

    public function store(Request $request){
        $request->validate([
            'name' => 'required|min:3',
            'email' => 'required|max:3',
            'password' => 'required',
        ]);

        DB::table('users')->insert([
            'name' => $request-> input('name'),
            'email' => $request-> input('email'),
            'password' => $request-> input('password')
        ]);

        return redirect('layouts.AppAdmin');
    }

}