<?php

namespace App\Http\Controllers;
use App\Models\Profile;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Contracts\Auth\Authenticatable;

class AuthController extends Controller
{
    public function login(){
        if(Auth::check()){
            return redirect()->route('layouts.appAdmin');
        }
        return view('login');
    }
 
    public function POST(Request $request){
        $credentials = [
            'username' => $request->username,
            'password' => $request->password
        ];
 
        if(Auth::attempt($credentials)){
            return redirect()->route('layouts.appAdmin');
        }
        return back()->with('error', 'Salah pada username ataupun password!');
    }
 
    public function register(){
        if(Auth::check()){
            return redirect()->route('layouts.appAdmin');
        }
        return view('register');
    }
 
    public function postRegister(Request $request){
        $check_email = User::where('email', $request->email)->first();
        if($check_email){
            return back()->with('error', 'Email sudah terdaftar!!');
        }
 
        $profile = Profile::create([
            'age'=> $request['age'],
            'bio'=> $request['bio'],
            'address'=> $request['address'],
            'genre'=> $request['genre'],
        ]);
        User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
            'profile_id'=> $profile->id
        ]);

        $credentials = [
            'name' => $profile->name,
            'password' => $request->password,
        ];
 
        if(Auth::attempt($credentials)){
            return redirect()->route('dashboard');
            
        }

        return view('dashboard');
    }
 
    public function logout(){
        Auth::logout();
        return redirect()->route('login');
    }
}
