<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    use HasFactory;
    protected $table = 'items';
    protected $fillable = ['name', 'description', 'price', 'thumbnail', 'stock', 'datePosted', 'category_id'];
}
