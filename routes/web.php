<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ItemController;
use App\Http\Controllers\ItemsController;
use App\Http\Controllers\QuickController;
use App\Http\Controllers\StoreController;
use App\Http\Controllers\TransactionController;
use App\Http\Controllers\UsersController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CategoriContreller;
use App\Http\Controllers\StoreContreller;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [QuickController::class, 'guest']); // pertama kali dijalankan
Route::get('/register', [QuickController::class, 'register']);
Route::get('/login', [QuickController::class, 'login']);
Auth::routes();


Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/about', [QuickController::class,'about']);

Route::get('/dashboard', [AdminController::class,'dashboard']);

// kategori 
Route::get('/kategori', [CategoriContreller::class,'index']);
Route::get('/kategori',[CategoriContreller::class, 'index']); // untuk menampilkan seluruh data
Route::get('/kategori/create',[CategoriContreller::class, 'tambah']); // untuk menambah data
Route::post('/kategori',[CategoriContreller::class, 'store']); // untuk menyimpan data baru
Route::get('/kategori/{category_id}',[CategoriContreller::class, 'show']); // untuk menampilkan data berdasarkan id tertentu
Route::get('/kategori/{category_id}/edit',[CategoriContreller::class, 'edit']); // untuk mengedit data berdasarkan id tertentu
Route::put('/kategori/{category_id}',[CategoriContreller::class, 'update']); // untuk mengupdate data berdasarkan id tertentu
Route::delete('/kategori/{category_id}',[CategoriContreller::class, 'destroy']); // untuk mengupdate data berdasarkan id tertentu


Route::post('/register', [AuthController::class,'postRegister']);

Route::resource('users', UsersController::class );

//Category

Route::get('/category/addcategory', [CategoryController::class,'create']);

Route::post('/category', [CategoryController::class,'store']);


Route::get('/category', [CategoryController::class, 'index']);

Route::get('/category{id}', [CategoryController::class, 'show']);

Route::get('/category/{id}/edit', [CategoryController::class, 'edit']);

Route::put('/category/{id}', [CategoryController::class,'update']);
Route::delete('/category/{id}', [CategoryController::class,'destroy']);

//Item

Route::get('/items/additem', [ItemController::class,'create']);

Route::post('/items', [ItemController::class,'store']);


Route::get('/items', [ItemController::class, 'index']);

Route::get('/items{id}', [ItemController::class, 'show']);

Route::get('/items/{id}/edit', [ItemController::class, 'edit']);

Route::put('/items/{id}', [ItemController::class,'update']);
Route::delete('/items/{id}', [ItemController::class,'destroy']);

// Route::resource('item',ItemsController::class);

//Transaction

Route::get('/transaction/addtransaction', [TransactionController::class,'create']);

Route::post('/transaction', [TransactionController::class,'store']);


Route::get('/transaction', [TransactionController::class, 'index']);

Route::get('/transaction{id}', [TransactionController::class, 'show']);

Route::get('/transaction/{id}/edit', [TransactionController::class, 'edit']);

Route::put('/transaction/{id}', [TransactionController::class,'update']);
Route::delete('/transaction/{id}', [TransactionController::class,'destroy']);

// Store

Route::get('/store/addstore', [TransactionController::class,'create']);

Route::post('/store', [TransactionController::class,'store']);


Route::get('/store', [TransactionController::class, 'index']);

Route::get('/store{id}', [TransactionController::class, 'show']);

Route::get('/store/{id}/edit', [TransactionController::class, 'edit']);

Route::put('/store/{id}', [TransactionController::class,'update']);
Route::delete('/store/{id}', [TransactionController::class,'destroy']);