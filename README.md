<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## <h1>Final Project</h1>

## Kelompok 23

## Anggota Kelompok
- Firda
- Justin
- Ias

## Tema Project
Sebuah e-commerce sederhana bernama SanberCommerce

## ERD
![ERD](<SanberCommerceERD.drawio (1).png>)

## Link Video
Link Demo : https://gitlab.com/Mimicry02/commercesanber/-/commit/19bf7c7033ba92cbac6d63e6537e196a1e5cd400
Link Deployment : commercesanber.bidangstudi.com